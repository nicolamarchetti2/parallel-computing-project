# Parallel Computing - Project

**Sequential Quicksort**

- Local machine:

    1. Compile: 'gcc -Ox sequential-quicksort.c -o sequential-quicksort' .

    2. Execution: './sequential-quicksort <input_file> <output_file>' .

- HPC Capri:
    
    1. Execution: 'sbatch seq_quicksort.slurm' .

*****************************

**Parallel Quicksort**

- Local machine:

    1. Compile: 'mpicc -Ox parallel-quicksort.c -o parallel-quicksort' .
    
    2. Execution: 'mpirun -np <num_procs> parallel-quicksort <input_file> <output_file>' .

- HPC Capri:
    
    1. Execution: 'sbatch np2_quicksort.slurm' or 'np4_quicksort.slurm' or 'np8_quicksort.slurm' .

*****************************
