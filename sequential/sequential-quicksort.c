#include <stdlib.h>
#include <stdio.h>
#include <time.h>

/*
*   Function to swap two elements
*/
void swap(int *x, int *y) {

  int temp = *x;
  *x = *y;
  *y = temp;

}

/*
*   Partition using Hoare's Partitioning scheme
*/
int partition( int *arr, int first, int last) {

    int pivot = arr[first];
    int i = first - 1;
    int j = last + 1;
    
    while(1){
        do{
            i++;
        }while(arr[i] < pivot);
        do{
            j--;
        }while(arr[j] > pivot);
        if (i >= j){
            return j;
        }
        swap(&arr[i], &arr[j]);
    }

}

/*
*   Function for quicksort
*/
void quicksort(int *arr, int low, int high) {
    
    if (low < high){
        
        //Partitions
        int pivot = partition(arr, low, high);

        //Recursions
        quicksort(arr, low, pivot);
        quicksort(arr, pivot + 1, high);
        
    }

}
 
/*
*   Main function
*/
int main(int argc, char ** argv) {

  int * dataset = NULL;
  FILE * file = NULL;
  int size;
  clock_t t;

  if (argc!=3){
    printf("Error! Please follow these instructions: %s <input_file> <output_file>\n", argv[0]);
    exit(0);
  }
  
  //Read size of dataset from input file
  file = fopen(argv[1], "r");
  fscanf(file, "%d", &size);

  ///Read data from the input file and save it
  dataset = (int *)malloc(size * sizeof(int));
  for (int i = 0; i < size; i++){
    fscanf(file, "%d", &(dataset[i]));
  }
  fclose(file);

  // Initial time
  t = clock();

  //Perform quicksort
  quicksort(dataset, 0, size - 1);

  // End time
  t = clock() - t;

  // Execution time
  double e_time = ((double)t)/CLOCKS_PER_SEC;

  //Write results
  file = fopen(argv[2], "w");
  for (int i = 0; i < size; i++){
    fprintf(file, "%d\n", dataset[i]);
  }
  fclose(file);

  //Print time
  printf("Quicksort (size %d) executed in %f seconds\n", size, e_time);

  //End
  return 0;

}
