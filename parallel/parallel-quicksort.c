#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <mpi.h>

/*
*   Function to swap two elements
*/
void swap(int *x, int *y) {

  int temp = *x;
  *x = *y;
  *y = temp;

}

/*
*   Partition using Hoare's Partitioning scheme
*/
int partition( int *arr, int first, int last) {

    int pivot = arr[first];
    int i = first - 1;
    int j = last + 1;
    
    while(1){
        do{
            i++;
        }while(arr[i] < pivot);
        do{
            j--;
        }while(arr[j] > pivot);
        if (i >= j){
            return j;
        }
        swap(&arr[i], &arr[j]);
    }

}

/*
*   Function for quicksort
*/
void quicksort(int *arr, int low, int high) {
    
    if (low < high){

        //Partitions
        int pivot = partition(arr, low, high);

        //Recursions
        quicksort(arr, low, pivot);
        quicksort(arr, pivot + 1, high);

    }

}

/*
*   Function for merging two sorted arrays
*/
int * merge(int * arr1, int size1, int * arr2, int size2) {

  int * final = (int *)malloc((size1 + size2) * sizeof(int));
  int i = 0;
  int j = 0;
  
  for (int z = 0; z < size1 + size2; z++){
    if (i >= size1){
      final[z] = arr2[j];
      j++;
    }
    else if (j >= size2){
      final[z] = arr1[i];
      i++;
    }
    else if (arr1[i] < arr2[j]){
      final[z] = arr1[i];
      i++;
    }
    else{ 
      final[z] = arr2[j];
      j++;
    }
  }

  return final;

}

/*
*   Main function
*/
int main(int argc, char ** argv) {

  int size, num_procs, rank, chunk_size, own_chunk_size, rec_chunk_size, step;
  double e_time, s_comm, e_comm, c_time = 0;
  int * dataset = NULL;
  int * chunk = NULL;
  int * rec_chunk = NULL;
  FILE * file = NULL;
  MPI_Status status;
  
  if (argc!=3){
    printf("Error! Please follow these instructions: mpirun -np <num_procs> %s <input_file> <output_file>\n", argv[0]);
    exit(0);
  }

  //Initialize MPI              
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == 0){
    //Read size of dataset from input file
    file = fopen(argv[1], "r");
    fscanf(file, "%d", &size);

    //Read data from the input file and save it
    dataset = (int *)malloc(size * sizeof(int));
    for (int i = 0; i < size; i++){
      fscanf(file, "%d", &(dataset[i]));
    }
    fclose(file);
  }

  //Blocks all process until reach this point
  MPI_Barrier(MPI_COMM_WORLD);

  //Start timer
  e_time = - MPI_Wtime();
  s_comm = MPI_Wtime();

  //Broadcast the size to all the process from root process
  MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);

  //End communication time
  e_comm = MPI_Wtime();
  c_time = c_time + (e_comm - s_comm);

  //Compute chunk size
  chunk_size = size/num_procs;

  //Scatter chuck size to all process
  chunk = (int *)malloc(chunk_size * sizeof(int));
  s_comm = MPI_Wtime();
  MPI_Scatter(dataset, chunk_size, MPI_INT, chunk, chunk_size, MPI_INT, 0, MPI_COMM_WORLD);
  e_comm = MPI_Wtime();
  c_time = c_time + (e_comm - s_comm);
  free(dataset);
  dataset = NULL;

  own_chunk_size = chunk_size;

  //Perform quicksort
  quicksort(chunk, 0, own_chunk_size - 1);

  //Merging
  for (step = 1; step < num_procs; step = 2*step){

    //If rank is no multiple of 2*step, send chunk to rank-step and exit loop
    if (rank % (2*step) != 0){
      s_comm = MPI_Wtime();
      MPI_Send(chunk, own_chunk_size, MPI_INT, rank-step, 0, MPI_COMM_WORLD);
      e_comm = MPI_Wtime();
      c_time = c_time + (e_comm - s_comm);
      break;
    }

    //If rank is multiple of 2*step, merge in chunk from rank+step 
    if (rank+step < num_procs){
      //Compute size of chunk to be received
      rec_chunk_size = chunk_size * step;
      
      //Receive chunk
      rec_chunk = (int *)malloc(rec_chunk_size * sizeof(int));
      s_comm = MPI_Wtime();
      MPI_Recv(rec_chunk, rec_chunk_size, MPI_INT, rank+step, 0, MPI_COMM_WORLD, &status);
      e_comm = MPI_Wtime();
      c_time = c_time + (e_comm - s_comm);
      
      //Merge
      dataset = merge(chunk, own_chunk_size, rec_chunk, rec_chunk_size);
      free(chunk);
      free(rec_chunk);
      chunk = dataset;
      own_chunk_size = own_chunk_size + rec_chunk_size;
    }

  }

  //Stop timer
  e_time += MPI_Wtime();

  //Write results
  if (rank == 0){

    file = fopen(argv[2], "w");
    for (int i = 0; i < own_chunk_size; i++){
      fprintf(file, "%d\n", chunk[i]);
    }
    fclose(file);

    //Print time
    printf("Quicksort (size %d) executed on %d procs in %f seconds\n", size, num_procs, e_time);
    printf("Communication time: %f seconds\n", c_time);
    printf("Computation time: %f seconds\n", e_time-c_time);

  }

  //End
  MPI_Finalize();
  return 0;

}
